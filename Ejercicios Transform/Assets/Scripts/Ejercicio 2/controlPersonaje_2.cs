﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPersonaje_2 : MonoBehaviour {

    public bool isMoving = false;
    public bool isRotating = false;

    public Vector3 destinationPoint;
    public Vector3 targetRotation;

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            if (transform.position != destinationPoint)
            {
                transform.position = (Vector3.Lerp(transform.position, destinationPoint, 0.2f));
                return;
            }
            else
            {
                isMoving = false;
            }
            
        }

        if (isRotating)
        {
            if (transform.rotation.eulerAngles != targetRotation)
            {
                transform.rotation =Quaternion.Euler( (Vector3.Lerp(transform.rotation.eulerAngles, targetRotation, 0.2f)));
                return;
            }
            else
            {
                isRotating = false;
            }
           
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            avanza();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            retrocede();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            mueveDerecha();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            mueveIzquierda();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            rotaIzquierda();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            rotaDerecha();
        }
    }

    void avanza()
    {
        isMoving = true;

        destinationPoint = transform.forward + transform.position;
    }

    void retrocede()
    {
        isMoving = true;

        destinationPoint = transform.forward * -1 + transform.position;
    }

    void mueveDerecha()
    {
        isMoving = true;

        destinationPoint = transform.right + transform.position;
    }

    void mueveIzquierda()
    {
        isMoving = true;

        destinationPoint = transform.right * -1 + transform.position;
    }

    void rotaDerecha()
    {
        isRotating = true;

        targetRotation = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z);
    }

    void rotaIzquierda()
    {
        isRotating = true;

        targetRotation = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90, transform.rotation.eulerAngles.z);
    }
}
